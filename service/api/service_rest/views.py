from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json
from common.json import ModelEncoder
from .models import Technician, AutomobileVO, ServiceAppointment


class AutomobileVOEncoder(ModelEncoder):
    model = AutomobileVO
    properties = ["vin"]


class TechnicianEncoder(ModelEncoder):
    model = Technician
    properties = [
        "id",
        "name",
        "employee_number",
    ]


class ServiceApptEncoder(ModelEncoder):
    model = ServiceAppointment
    properties = [
        "id",
        "automobile_vin",
        "owner",
        "date",
        "time",
        "technician",
        "reason",
        "vip",
        "status",
    ]
    encoders = {
        "technician": TechnicianEncoder(),
    }


@require_http_methods(["GET", "POST"])
def api_technicians(request):
    if request.method == "GET":
        technicians = Technician.objects.all()
        return JsonResponse(
            {"technicians": technicians}, encoder=TechnicianEncoder
        )
    # POST
    else:
        content = json.loads(request.body)
        try:
            technician = Technician.objects.create(**content)
            return JsonResponse(
                technician,
                encoder=TechnicianEncoder,
                safe=False,
            )
        except Technician.DoesNotExist:
            return JsonResponse(
                {"message": "Technician does not exist"}, status=404
            )


@require_http_methods(["GET", "DELETE"])
def api_technician(request, pk):
    if request.method == "GET":
        try:
            technician = Technician.objects.get(id=pk)
            return JsonResponse(
                technician, encoder=TechnicianEncoder, safe=False
            )
        except Technician.DoesNotExist:
            return JsonResponse(
                {"message": "Technician does not exist"}, status=404
            )
    # DELETE
    else:
        count, _ = Technician.objects.filter(id=pk).delete()
        return JsonResponse({"deleted": count > 0})


@require_http_methods(["GET", "POST"])
def api_service_appointments(request):
    if request.method == "GET":
        filters = {}
        show_status = request.GET.get("status")
        show_vin = request.GET.get("vin")
        if show_status:
            filters["status"] = show_status
        if show_vin:
            filters["automobile_vin"] = show_vin
        appointments = ServiceAppointment.objects.filter(**filters)

        return JsonResponse(
            {"appointments": appointments}, encoder=ServiceApptEncoder
        )
    # POST
    else:
        content = json.loads(request.body)
        try:
            technician_id = content["technician"]
            technician = Technician.objects.get(id=technician_id)
            content["technician"] = technician
        except Technician.DoesNotExist:
            return JsonResponse(
                {"message": "Technician does not exist"}, status=404
            )
        try:
            vin = content["automobile_vin"]
            AutomobileVO.objects.get(vin=vin)
            content["vip"] = True
        except AutomobileVO.DoesNotExist:
            content["vip"] = False

        appointment = ServiceAppointment.objects.create(**content)
        return JsonResponse(
            appointment, encoder=ServiceApptEncoder, safe=False
        )


@require_http_methods(["GET", "PUT", "DELETE"])
def api_service_appointment(request, pk):
    if request.method == "GET":
        try:
            appointment = ServiceAppointment.objects.get(id=pk)
            return JsonResponse(
                appointment, encoder=ServiceApptEncoder, safe=False
            )
        except ServiceAppointment.DoesNotExist:
            return JsonResponse(
                {"message": "Appointment does not exist"}, status=404
            )
    elif request.method == "PUT":
        try:
            content = json.loads(request.body)
            appointment = ServiceAppointment.objects.get(id=pk)
            props = ["date", "time", "status"]
            for prop in props:
                if prop in content:
                    setattr(appointment, prop, content[prop])
            appointment.save()
            return JsonResponse(
                appointment,
                encoder=ServiceApptEncoder,
                safe=False,
            )
        except ServiceAppointment.DoesNotExist:
            return JsonResponse(
                {"message": "Appointment does not exist"}, status=404
            )
    # DELETE
    else:
        count, _ = ServiceAppointment.objects.filter(id=pk).delete()
        return JsonResponse({"deleted": count > 0})
