from django.urls import path
from .views import (
    api_technicians,
    api_technician,
    api_service_appointments,
    api_service_appointment,
)

urlpatterns = [
    path("technicians/", api_technicians, name="create_technician"),
    path("technicians/<int:pk>/", api_technician, name="show_technician"),
    path("appointments/", api_service_appointments, name="list_appointments"),
    path(
        "appointments/<int:pk>/",
        api_service_appointment,
        name="modify_appointment",
    ),
]
