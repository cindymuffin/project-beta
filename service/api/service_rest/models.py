from django.db import models
from django.urls import reverse

# Create your models here.
class Technician(models.Model):
    name = models.CharField(max_length=50)
    employee_number = models.PositiveIntegerField()

    def get_api_url(self):
        return reverse("api_technician", kwargs={"pk": self.id})


class AutomobileVO(models.Model):
    vin = models.CharField(max_length=17, unique=True)
    import_href = models.CharField(max_length=200, unique=True)


class ServiceAppointment(models.Model):
    automobile_vin = models.CharField(max_length=17)
    owner = models.CharField(max_length=50)
    date = models.DateField()
    time = models.TimeField()
    technician = models.ForeignKey(
        Technician,
        related_name="technician",
        on_delete=models.CASCADE,
    )
    reason = models.CharField(max_length=200)
    vip = models.BooleanField(default=False)

    class Status(models.TextChoices):
        CANCELLED = "Cancelled"
        COMPLETE = "Complete"
        PENDING = "Pending"

    status = models.CharField(
        max_length=20,
        choices=Status.choices,
        default=Status.PENDING,
    )

    def get_api_url(self):
        return reverse("api_service_appointment", kwargs={"pk": self.id})
