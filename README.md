# CarCar

Team:

- Elliott Klaassen - Sales
- Cindy Luu - Service

## Design

We determined there are three Bounded Contexts in our problem domain.

Inventory:
Within the Inventory there are three models - Manufacturer, VehicleModel, and Automobile. The Automobile Inventory is the main root connecting all microservices together.

Service:
Within Service there are also three models - Technician, ServiceAppointment, and AutomobileVO. AutomobileVO is the particular model connected to the Inventory. With AutomobileVO, we poll to the Inventory's Automobile model for existing VINs in the database in order to determine whether the car/owner will recieve VIP treatment when coming in for service.

Sales:
Within the Sales microservice there are four models - Sales Person, Customer, Sales Record, and the AutomobileVO. AutomobileVO is the particular VO connected to the Autmobile models in the Inventory. With AutomobileVO, the Sales microservice poller checks the Inventory API's Automobile instances for the VIN property on those instances in order to match it to the VIN property of the AutomobileVO to determine whether the car has been associated with a sales record by the sales department yet.

## Service Microservice (Cindy)

For the service, I created the models based off of the Learn requirments (Technician, ServiceAppointment, and an AutomobileVO to grab the VIN from the Inventory Automobile model) but later realized that within the ServiceAppointment, I wanted to add a VIP property and a status property. The VIP property would be a boolean field which would determine whether the VIN was an exisiting VIN within the inventory in order to get the owner VIP treatment once coming in for service and would show in the Appointments List. The VIP property would be set to True if when POSTing an appointment, it could find the VIN using AutomobileVO (which would poll from Inventory) and if it couldn't find the VIN, it would create the VIN with the VIP property set as False. The status property would determine the status of each appointment, whether it was pending, cancelled or complete. Within my views.py, I was able to make an if statement inside my api_service_appointments which would provide query links for my Appointments List (query for pending apppointments so that they could either be cancelled or completed via the Appointments List) and for Service History (query for specific VIN and grab it's history). On the Appointments List page, the cancelled and finished buttons would change the status of the appointment once clicked, but not remove them from the database so as to when searching for a VIN's history, we could see a complete list of all appointments, whether pending, complete, or cancelled.

## Sales Microservice (Elliott)

For sales, I created the models based off of the Learn requirements for an AutomobileVO, Sales Person, Customer, and Sales Record models. I hooked up my view functions to properly pull data from the inventory to link up with the sales restful api I built out myself to also meet the requirements on Learn. Then, I made React components to the specs on Learn to manipulate and interact with all the data generated and polled by the backend work I set up.

I struggled for a long time with how I should designate an Automobile as having been sold by the Sales team. Ultimately, I could not figure out a clean solution to adjust the Inventory API models and have that relate to my Sales API VO models, so I opted for a solution on the front end. I am pretty happy with the way that I filtered autos from the salesrecords database from the dropdown in the sales record form.

I was also proud of the way that I filtered an array of sales records associated with a sales employee's employee number in the sales history form. That was a neat bit of engineering that felt good to get working once I had it. It took me far too long to realize that my componentDidMount function was setting the initial state of the page to render ALL sales by all employees, but eventually I realized that I needed to set that to an empty list, and then everything was working. When a sales representative is selected from the dropdown, only their respective sales tied to their number will render in a table below the dropdown.

I threw some bonuses into a few of the inventory front-end pages that I worked on; on the Manufacturer List and the Models List page, there is an option to delete either of these in case CarCar no longer stocks those models or cars from that manufacturer. I was careful to make sure that this in turn did not affect the records of auto sales in the sales history page; deleting a model will not delete a record of those models being sold previously by CarCar, it just removes them as an option to add new Automobiles by VIN. Of course, if CarCar begins stocking that model or Manufacturer again, all they need to do is simply re-create those instances with the respective form provided.
