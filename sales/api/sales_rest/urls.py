from django.urls import path

from .views import (
    list_salespersons,
    show_salesperson,
    list_customers,
    show_customer,
    list_salesrecords,
    show_salesrecord
)


urlpatterns = [
    path("salespersons/", list_salespersons, name="list_salespersons"),
    path("salespersons/<int:pk>/", show_salesperson, name="show_salesperson"),
    path("customers/", list_customers, name="list_customers"),
    path("customers/<str:phone_number>/", show_customer, name="show_customer"),
    path("salesrecords/", list_salesrecords, name="list_salesrecords"),
    path("salesrecords/<int:pk>/", show_salesrecord, name="show_salesrecord")
]