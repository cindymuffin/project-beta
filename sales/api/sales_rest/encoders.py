from common.json import ModelEncoder

from .models import AutomobileVO, Customer, SalesPerson, SalesRecord


class AutomobileVOEncoder(ModelEncoder):
    model = AutomobileVO
    properties = ["vin", "import_href"]


class CustomerEncoder(ModelEncoder):
    model = Customer
    properties = [
        "id", 
        "name", 
        "address", 
        "phone_number"
    ]


class SalesPersonEncoder(ModelEncoder):
    model = SalesPerson
    properties = [
        "id", 
        "name", 
        "employee_number"
    ]


class SalesRecordEncoder(ModelEncoder):
    model = SalesRecord
    properties = [
        "id",
        "price", 
        "automobile", 
        "sales_person", 
        "customer"
    ]
    encoders = {
        "automobile": AutomobileVOEncoder(),
        "customer": CustomerEncoder(),
        "sales_person": SalesPersonEncoder(),
    }

    def get_extra_data(self, o):
        return {
            "automobile": o.automobile.vin, 
            "sales_person": {
                "name": o.sales_person.name,
                "employee_number": o.sales_person.employee_number,
            }, 
            "customer": o.customer.name,
            "price": float(o.price)
        } 
