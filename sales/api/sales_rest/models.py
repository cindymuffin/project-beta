from django.db import models
from django.urls import reverse


class AutomobileVO(models.Model):
    import_href = models.CharField(max_length=200, unique=True)
    vin = models.CharField(max_length=17, unique=True)
    # sold = models.BooleanField(default=False)

    def __str__(self):
        return f"{self.vin}"


class SalesPerson(models.Model):
    name = models.CharField(max_length=50)
    employee_number = models.PositiveIntegerField(unique=True)

    def __str__(self):
        return self.name

    def get_api_url(self):
        return reverse("show_salesperson", kwargs={"pk": self.id})


class Customer(models.Model):
    name = models.CharField(max_length=50)
    address = models.CharField(max_length=200)
    phone_number = models.CharField(max_length=12, unique=True)

    def __str__(self):
        return self.name

    def get_api_url(self):
        return reverse("show_customer", kwargs={"phone_number": self.phone_number})


class SalesRecord(models.Model):
    price = models.DecimalField(max_digits=8, decimal_places=2)

    automobile = models.ForeignKey(
        AutomobileVO,
        related_name="automobile",
        on_delete=models.PROTECT
    )

    sales_person = models.ForeignKey(
        SalesPerson,
        related_name="sales_person",
        on_delete=models.PROTECT
    )

    customer = models.ForeignKey(
        Customer,
        related_name="customer",
        on_delete=models.PROTECT
    )

    def __str__(self):
        return f"Sales Record by {self.sales_person.name} for {self.automobile.vin}"

    def get_api_url(self):
        return reverse("show_salesrecord", kwargs={"pk": self.id})


