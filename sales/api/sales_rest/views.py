from django.shortcuts import render
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json

from .encoders import (
    AutomobileVOEncoder,
    CustomerEncoder,
    SalesPersonEncoder,
    SalesRecordEncoder,
)
from .models import (
    AutomobileVO,
    Customer,
    SalesPerson,
    SalesRecord,
)


@require_http_methods(["GET", "POST"])
def list_salespersons(request):
    if request.method == "GET":
        sales_persons = SalesPerson.objects.all()
        return JsonResponse(
            {"sales_persons": sales_persons},
            encoder=SalesPersonEncoder,
        )
    # POST
    else:
        try:
            content = json.loads(request.body)
            sales_person = SalesPerson.objects.create(**content)
            return JsonResponse(
                sales_person,
                encoder=SalesPersonEncoder,
                safe=False,
            )
        except:
            response = JsonResponse(
                {
                    "message": "Could not create the Sales Person. Try using a unique employee number."
                }
            )
            response.status_code = 400
            return response


@require_http_methods(["GET", "DELETE", "PUT"])
def show_salesperson(request, pk):
    if request.method == "GET":
        try:
            sales_person = SalesPerson.objects.get(id=pk)
            return JsonResponse(
                sales_person,
                encoder=SalesPersonEncoder,
                safe=False,
            )
        except SalesPerson.DoesNotExist:
            response = JsonResponse({"message": "Sales Person does not exist"})
            response.status_code = 404
            return response
    elif request.method == "DELETE":
        try:
            count, _ = SalesPerson.objects.filter(id=pk).delete()
            return JsonResponse({"deleted": count > 0})
        except SalesPerson.DoesNotExist:
            return JsonResponse({"message": "Sales Person does not exist"})
    # PUT
    else:
        try:
            content = json.loads(request.body)
            SalesPerson.objects.filter(id=pk).update(**content)
            sales_person = SalesPerson.objects.get(id=pk)
            return JsonResponse(
                sales_person,
                encoder=SalesPersonEncoder,
                safe=False,
            )
        except SalesPerson.DoesNotExist:
            response = JsonResponse({"message": "Sales Person does not exist"})
            response.status_code = 404
            return response


@require_http_methods(["GET", "POST"])
def list_customers(request):
    if request.method == "GET":
        customers = Customer.objects.all()
        return JsonResponse(
            {"customers": customers},
            encoder=CustomerEncoder,
        )
    # POST
    else:
        try:
            content = json.loads(request.body)
            customer = Customer.objects.create(**content)
            return JsonResponse(
                customer,
                encoder=CustomerEncoder,
                safe=False,
            )
        except:
            response = JsonResponse(
                {
                    "message": "Could not create the Customer. Try using a unique phone number."
                }
            )
            response.status_code = 400
            return response


@require_http_methods(["GET", "PUT", "DELETE"])
def show_customer(request, phone_number):
    if request.method == "GET":
        try:
            customer = Customer.objects.get(phone_number=phone_number)
            return JsonResponse(
                customer,
                encoder=CustomerEncoder,
                safe=False,
            )
        except Customer.DoesNotExist:
            response = JsonResponse({"message": "Customer does not exist"})
            response.status_code = 404
            return response
    elif request.method == "DELETE":
        try:
            count, _ = Customer.objects.filter(phone_number=phone_number).delete()
            return JsonResponse({"deleted": count > 0})
        except Customer.DoesNotExist:
            return JsonResponse({"message": "Customer does not exist"})
    # PUT
    else:
        try:
            content = json.loads(request.body)
            Customer.objects.filter(phone_number=phone_number).update(**content)
            customer = Customer.objects.get(phone_number=phone_number)
            return JsonResponse(
                customer,
                encoder=CustomerEncoder,
                safe=False,
            )
        except Customer.DoesNotExist:
            response = JsonResponse({"message": "Customer does not exist"})
            response.status_code = 404
            return response


@require_http_methods(["GET", "POST"])
def list_salesrecords(request):
    if request.method == "GET":
        sales_records = SalesRecord.objects.all()
        return JsonResponse(
            {"sales_records": sales_records},
            encoder=SalesRecordEncoder,
        )
    # POST
    else:
        content = json.loads(request.body)

        # Before posting, check to see that the sales representative on the form exists.
        # Here, we are checking by the automobile's VIN, as defined by the
        # SalesRecordEncoder in encoders.py.
        try:
            vin = content["automobile"]
            automobile = AutomobileVO.objects.get(vin=vin)
            content["automobile"] = automobile
        except AutomobileVO.DoesNotExist:
            response = JsonResponse({"message": "Automobile does not exist"})
            response.status_code = 404
            return response

        # Before posting, check to see that the sales representative on the form exists.
        # Here, we are checking by the sales representative's name, as defined
        # by the SalesRecordEncoder in encoders.py.
        try:
            name = content["sales_person"]
            sales_person = SalesPerson.objects.get(name=name)
            content["sales_person"] = sales_person
        except SalesPerson.DoesNotExist:
            response = JsonResponse({"message": "Sales Person does not exist"})
            response.status_code = 404
            return response

        # Before posting, check to see that the customer on the form exists.
        # Here, we are checking by the customer's name, as defined by the
        # SalesRecordEncoder in encoders.py.
        try:
            name = content["customer"]
            customer = Customer.objects.get(name=name)
            content["customer"] = customer
        except Customer.DoesNotExist:
            response = JsonResponse({"message": "Customer does not exist"})
            response.status_code = 404
            return response

        sales_record = SalesRecord.objects.create(**content)
        return JsonResponse(
            sales_record,
            encoder=SalesRecordEncoder,
            safe=False,
        )


@require_http_methods(["GET", "PUT", "DELETE"])
def show_salesrecord(request, pk):
    if request.method == "GET":
        try:
            sales_record = SalesRecord.objects.get(id=pk)
            return JsonResponse(
                sales_record,
                encoder=SalesRecordEncoder,
                safe=False,
            )
        except SalesRecord.DoesNotExist:
            response = JsonResponse({"message": "Sales Record does not exist"})
            response.status_code = 404
            return response
    elif request.method == "DELETE":
        try:
            count, _ = SalesRecord.objects.filter(id=pk).delete()
            return JsonResponse({"deleted": count > 0})
        except SalesRecord.DoesNotExist:
            return JsonResponse({"message": "Sales Record does not exist"})
    # PUT
    else:
        try:
            content = json.loads(request.body)

            try:
                vin = content["automobile"]
                automobile = AutomobileVO.objects.get(vin=vin)
                content["automobile"] = automobile
            except AutomobileVO.DoesNotExist:
                response = JsonResponse({"message": "Automobile does not exist"})
                response.status_code = 404
                return response

            try:
                name = content["sales_person"]
                sales_person = SalesPerson.objects.get(name=name)
                content["sales_person"] = sales_person
            except SalesPerson.DoesNotExist:
                response = JsonResponse({"message": "Sales Person does not exist"})
                response.status_code = 404
                return response

            try:
                name = content["customer"]
                customer = Customer.objects.get(name=name)
                content["customer"] = customer
            except Customer.DoesNotExist:
                response = JsonResponse({"message": "Customer does not exist"})
                response.status_code = 404
                return response

            SalesRecord.objects.filter(id=pk).update(**content)
            sales_record = SalesRecord.objects.get(id=pk)
            return JsonResponse(
                sales_record,
                encoder=SalesRecordEncoder,
                safe=False,
            )
        except SalesRecord.DoesNotExist:
            response = JsonResponse({"message": "Sales Record does not exist"})
            response.status_code = 404
            return response
