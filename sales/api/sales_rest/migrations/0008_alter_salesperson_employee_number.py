# Generated by Django 4.0.3 on 2022-05-11 04:36

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('sales_rest', '0007_alter_customer_phone_number'),
    ]

    operations = [
        migrations.AlterField(
            model_name='salesperson',
            name='employee_number',
            field=models.PositiveIntegerField(unique=True),
        ),
    ]
