import { BrowserRouter, Routes, Route } from "react-router-dom";
import MainPage from "./MainPage";
import Nav from "./Nav";
import AutomobilesList from "./inventory/AutomobilesList";
import AutomobileForm from "./inventory/AutomobileForm";
import ManufacturersList from "./inventory/ManufacturersList";
import ManufacturerForm from "./inventory/ManufacturerForm";
import ModelsList from "./inventory/ModelsList";
import ModelForm from "./inventory/ModelForm";
import AppointmentsList from "./services/AppointmentsList";
import AppointmentForm from "./services/AppointmentForm";
import TechnicianForm from "./services/TechnicianForm";
import ServiceHistory from "./services/ServiceHistory";
import SalesPersonForm from "./sales/SalesPersonForm";
import CustomerForm from "./sales/CustomerForm";
import SalesRecordForm from "./sales/SalesRecordForm";
import SalesList from "./sales/SalesList";
import SalesHistory from "./sales/SalesHistory";

function App(props) {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="manufacturers">
            <Route path="" element={<ManufacturersList />} />
            <Route path="new" element={<ManufacturerForm />} />
          </Route>
          <Route path="models">
            <Route path="" element={<ModelsList />} />
            <Route path="new" element={<ModelForm />} />
          </Route>
          <Route path="automobiles">
            <Route path="" element={<AutomobilesList />} />
            <Route path="new" element={<AutomobileForm />} />
          </Route>
          <Route path="appointments">
            <Route path="" element={<AppointmentsList />} />
            <Route path="new" element={<AppointmentForm />} />
          </Route>
          <Route path="technician" element={<TechnicianForm />} />
          <Route path="servicehistory" element={<ServiceHistory />} />
          <Route path="salespersons" element={<SalesPersonForm />} />
          <Route path="customers" element={<CustomerForm />} />
          <Route path="salesrecords" element={<SalesRecordForm />} />
          <Route path="sales" element={<SalesList />} />
          <Route path="saleshistory" element={<SalesHistory />} />
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
