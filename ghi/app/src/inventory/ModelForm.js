import React, { useEffect, useState } from "react";

function ModelForm() {
  const [name, setName] = useState("");
  const [picture_url, setPictureUrl] = useState("");
  const [manufacturer_id, setManufacturer] = useState("");
  const [manufacturers, setManufacturers] = useState([]);

  useEffect(() => {
    const getManufacturerData = async () => {
      const manufacturerResponse = await fetch("http://localhost:8100/api/manufacturers/");
      const manufacturerData = await manufacturerResponse.json();
      setManufacturers(manufacturerData.manufacturers);
    };

    getManufacturerData();
  }, []);

  const handleSubmit = async (event) => {
    event.preventDefault();
    const data = {
      name,
      picture_url,
      manufacturer_id
    };
    const modelsUrl = "http://localhost:8100/api/models/";
    const fetchConfig = {
      method: "POST",
      body: JSON.stringify(data),
      headers: {
        "Content-Type": "application/json",
      },
    };
    const response = await fetch(modelsUrl, fetchConfig);

    if (response.ok) {
      setName("");
      setPictureUrl("");
      setManufacturer("");
    }
  };

  const nameChange = (event) => {
    setName(event.target.value);
  };

  const pictureUrlChange = (event) => {
    setPictureUrl(event.target.value);
  };

  const manufacturerChange = (event) => {
    setManufacturer(event.target.value);
  };

  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Add a new vehicle model</h1>
          <form
            onSubmit={handleSubmit}
            id="create-model-form"
          >
            <div className="form-floating mb-3">
              <input
                className="form-control"
                onChange={nameChange}
                value={name}
                placeholder="Model name"
                required
                type="text"
                name="name"
                id="name"
              />
              <label htmlFor="name">Model Name</label>
            </div>
            <div className="form-floating mb-3">
              <input
                className="form-control"
                onChange={pictureUrlChange}
                value={picture_url}
                placeholder="Picture URL"
                required
                type="text"
                name="picture_url"
                id="picture_url"
              />
              <label htmlFor="picture_url">Picture URL</label>
            </div>
            <div className="mb-3">
              <select
                onChange={manufacturerChange}
                value={manufacturer_id}
                required
                id="manufacturer"
                name="manufacturer"
                className="form-select"
              >
                <option value="">Choose a manufacturer</option>
                {manufacturers.map((manufacturer) => {
                  return (
                    <option key={manufacturer.id} value={manufacturer.id}>
                      {manufacturer.name}
                    </option>
                  );
                })}
              </select>
            </div>
            <button className="btn btn-primary">Create</button>
          </form>
        </div>
      </div>
    </div>
  )
}

export default ModelForm;


// import React from "react";

// class ModelForm extends React.Component {
//   constructor(props) {
//     super(props);
//     this.state = {
//       name: "",
//       pictureUrl: "",
//       manufacturers: [],
//       manufacturer: "",
//       modelCreated: false,
//     };
//     this.handleChange = this.handleChange.bind(this);
//     this.handlePictureUrlChange = this.handlePictureUrlChange.bind(this);
//     this.handleSubmit = this.handleSubmit.bind(this);
//   }

//   handleChange(event) {
//     const newState = {};
//     newState[event.target.id] = event.target.value;
//     this.setState(newState);
//   }

//   handlePictureUrlChange(event) {
//     const value = event.target.value;
//     this.setState({ pictureUrl: value });
//   }

//   async handleSubmit(event) {
//     event.preventDefault();
//     const data = { ...this.state };
//     data.picture_url = data.pictureUrl;
//     delete data.pictureUrl;
//     data["manufacturer_id"] = data.manufacturer;
//     delete data.manufacturer;
//     delete data.manufacturers;
//     delete data.modelCreated;

//     const modelUrl = "http://localhost:8100/api/models/";
//     const fetchConfig = {
//       method: "POST",
//       body: JSON.stringify(data),
//       headers: {
//         "Content-Type": "application/json",
//       },
//     };

//     const response = await fetch(modelUrl, fetchConfig);

//     if (response.ok) {
//       this.setState({
//         name: "",
//         pictureUrl: "",
//         manufacturer: "",
//         modelCreated: true,
//       });
//     }
//   }

//   async componentDidMount() {
//     const url = "http://localhost:8100/api/manufacturers/";
//     const response = await fetch(url);

//     if (response.ok) {
//       const data = await response.json();
//       this.setState({ manufacturers: data.manufacturers });
//     }
//   }

//   render() {
//     let messageClasses = "alert alert-success d-none mb-0";
//     let formClasses = "";
//     if (this.state.modelCreated) {
//       messageClasses = "alert alert-success mb-0";
//       formClasses = "d-none";
//     }

//     return (
//       <div className="row">
//         <div className="offset-3 col-6">
//           <div className="shadow p-4 mt-4">
//             <h1>Add a new vehicle model</h1>
//             <form
//               className={formClasses}
//               onSubmit={this.handleSubmit}
//               id="create-model-form"
//             >
//               <div className="form-floating mb-3">
//                 <input
//                   className="form-control"
//                   onChange={this.handleChange}
//                   value={this.state.name}
//                   placeholder="Model name"
//                   required
//                   type="text"
//                   name="name"
//                   id="name"
//                 />
//                 <label htmlFor="name">Model Name</label>
//               </div>
//               <div className="form-floating mb-3">
//                 <input
//                   className="form-control"
//                   onChange={this.handlePictureUrlChange}
//                   value={this.state.pictureUrl}
//                   placeholder="Picture URL"
//                   required
//                   type="text"
//                   name="picture_url"
//                   id="picture_url"
//                 />
//                 <label htmlFor="picture_url">Picture URL</label>
//               </div>
//               <div className="mb-3">
//                 <select
//                   onChange={this.handleChange}
//                   value={this.state.manufacturer}
//                   required
//                   id="manufacturer"
//                   name="manufacturer"
//                   className="form-select"
//                 >
//                   <option value="">Choose a manufacturer</option>
//                   {this.state.manufacturers.map((manufacturer) => {
//                     return (
//                       <option key={manufacturer.id} value={manufacturer.id}>
//                         {manufacturer.name}
//                       </option>
//                     );
//                   })}
//                 </select>
//               </div>
//               <button className="btn btn-primary">Create</button>
//             </form>
//             <div className={messageClasses} id="success-message">
//               You have added a new car model!
//             </div>
//           </div>
//         </div>
//       </div>
//     );
//   }
// }

// export default ModelForm;
