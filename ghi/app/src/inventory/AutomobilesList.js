import React, { useEffect, useState } from "react";

function AutomobilesList() {
  const [autos, setAutos] = useState([])

  useEffect(() => {
    const getAutomobileData = async () => {
      const autoResponse = await fetch("http://localhost:8100/api/automobiles/");
      const autoData = await autoResponse.json();
      setAutos(autoData.autos);
    };

    getAutomobileData();
  }, []);

  return (
    <>
      <div className="px-4 py-5 my-5 text-center">
        <h1 className="display-5 fw-bold">Automobiles</h1>
        <table className="table table-striped">
          <thead>
            <tr>
              <th scope="col">VIN</th>
              <th scope="col">Color</th>
              <th scope="col">Year</th>
              <th scope="col">Model</th>
              <th scope="col">Manufacturer</th>
            </tr>
          </thead>
          <tbody>
            {autos.map((auto) => {
              return (
                <tr key={auto.vin}>
                  <td>{auto.vin}</td>
                  <td>{auto.color}</td>
                  <td>{auto.year}</td>
                  <td>{auto.model.name}</td>
                  <td>{auto.model.manufacturer.name}</td>
                </tr>
              );
            })}
          </tbody>
        </table>
      </div>
    </>
  );
}

export default AutomobilesList;

// import React from "react";

// function AutomobilesTable(props) {
//   return (
//     <tr key={props.auto.id}>
//       <td>{props.auto.vin}</td>
//       <td>{props.auto.color}</td>
//       <td>{props.auto.year}</td>
//       <td>{props.auto.model.name}</td>
//       <td>{props.auto.model.manufacturer.name}</td>
//     </tr>
//   );
// }

// class AutomobilesList extends React.Component {
//   constructor(props) {
//     super(props);
//     this.state = {
//       autos: [],
//     };
//   }

//   async componentDidMount() {
//     const url = "http://localhost:8100/api/automobiles/";

//     const response = await fetch(url);
//     const data = await response.json();

//     if (response.ok) {
//       this.setState({ autos: data.autos });
//     }
//   }

//   render() {
//     return (
//       <>
//         <div className="px-4 py-5 my-5 text-center">
//           <h1 className="display-5 fw-bold">Automobiles</h1>
//           <table className="table table-striped">
//             <thead>
//               <tr>
//                 <th scope="col">VIN</th>
//                 <th scope="col">Color</th>
//                 <th scope="col">Year</th>
//                 <th scope="col">Model</th>
//                 <th scope="col">Manufacturer</th>
//               </tr>
//             </thead>
//             <tbody>
//               {this.state.autos.map((auto) => {
//                 return <AutomobilesTable auto={auto} key={auto.id} />;
//               })}
//             </tbody>
//           </table>
//         </div>
//       </>
//     );
//   }
// }

// export default AutomobilesList;
