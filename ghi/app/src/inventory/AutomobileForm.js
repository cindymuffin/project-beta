import React, { useEffect, useState } from "react";

function AutomobileForm() {
  const [color, setColor] = useState("");
  const [year, setYear] = useState("");
  const [vin, setVin] = useState("");
  const [model_id, setModel] = useState("");
  const [models, setModels] = useState([]);

  useEffect(() => {
    const getAutomobileData = async () => {
      const autoResponse = await fetch("http://localhost:8100/api/models/");
      const autoData = await autoResponse.json();
      setModels(autoData.models);
    };

    getAutomobileData();
  }, []);

  const handleSubmit = async (event) => {
    event.preventDefault();
    const data = {
      color,
      year,
      vin,
      model_id,
    };
    const automobilesUrl = "http://localhost:8100/api/automobiles/";
    const fetchConfig = {
      method: "POST",
      body: JSON.stringify(data),
      headers: {
        "Content-Type": "application/json",
      },
    };
    const response = await fetch(automobilesUrl, fetchConfig);

    if (response.ok) {
      setColor("");
      setYear("");
      setVin("");
      setModel("");
    }
  };

  const colorChange = (event) => {
    setColor(event.target.value);
  };

  const yearChange = (event) => {
    setYear(event.target.value);
  };

  const vinChange = (event) => {
    setVin(event.target.value);
  };

  const modelChange = (event) => {
    setModel(event.target.value);
  };

  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Add a new automobile</h1>
            <form
              onSubmit={handleSubmit}
              id="create-automobile-form"
            >
              <div className="form-floating mb-3">
                <input
                  onChange={colorChange}
                  value={color}
                  placeholder="Color"
                  required
                  type="text"
                  name="color"
                  id="color"
                  className="form-control"
                />
                <label htmlFor="color">Color</label>
              </div>
              <div className="form-floating mb-3">
                <input
                  onChange={yearChange}
                  value={year}
                  placeholder="Year"
                  required
                  type="text"
                  name="year"
                  id="year"
                  className="form-control"
                />
                <label htmlFor="year">Year</label>
              </div>
              <div className="form-floating mb-3">
                <input
                  onChange={vinChange}
                  value={vin}
                  placeholder="Vin"
                  required
                  type="text"
                  name="vin"
                  id="vin"
                  className="form-control"
                />
                <label htmlFor="vin">Vin</label>
              </div>
              <div className="mb-3">
                <select
                  onChange={modelChange}
                  value={model_id}
                  required
                  name="model"
                  id="model"
                  className="form-select"
                >
                  <option value="">Choose a model</option>
                  {models.map((model) => {
                    return (
                      <option key={model.id} value={model.id}>
                        {model.manufacturer.name} {model.name}
                      </option>
                    );
                  })}
                </select>
              </div>
              <button className="btn btn-primary">Add automobile</button>
            </form>
        </div>
      </div>
    </div>
  );
}

export default AutomobileForm;

// import React from "react";

// class AutomobileForm extends React.Component {
//   constructor(props) {
//     super(props);
//     this.state = {
//       color: "",
//       year: "",
//       vin: "",
//       model: "",
//       models: [],
//       autoCreated: false,
//     };
//     this.handleChange = this.handleChange.bind(this);
//     // this.handleColorChange = this.handleColorChange.bind(this);
//     // this.handleYearChange = this.handleYearChange.bind(this);
//     // this.handleVinChange = this.handleVinChange.bind(this);
//     // this.handleModelChange = this.handleModelChange.bind(this);
//     this.handleSubmit = this.handleSubmit.bind(this);
//   }

//   handleChange(event) {
//     const newState = {};
//     newState[event.target.id] = event.target.value;
//     this.setState(newState);
//   }

//   // handleColorChange(event) {
//   //   const value = event.target.value;
//   //   this.setState({ color: value });
//   // }

//   // handleYearChange(event) {
//   //   const value = event.target.value;
//   //   this.setState({ year: value });
//   // }

//   // handleVinChange(event) {
//   //   const value = event.target.value;
//   //   this.setState({ vin: value });
//   // }

//   // handleModelChange(event) {
//   //   const value = event.target.value;
//   //   this.setState({ model: value });
//   // }

//   async handleSubmit(event) {
//     event.preventDefault();
//     const data = { ...this.state };
//     data["model_id"] = data.model;
//     delete data.model;
//     delete data.models;
//     delete data.autoCreated;

//     const automobilesUrl = "http://localhost:8100/api/automobiles/";
//     const fetchConfig = {
//       method: "POST",
//       body: JSON.stringify(data),
//       headers: {
//         "Content-Type": "application/json",
//       },
//     };

//     const response = await fetch(automobilesUrl, fetchConfig);
//     if (response.ok) {
//       this.setState({
//         color: "",
//         year: "",
//         vin: "",
//         model: "",
//         autoCreated: true,
//       });
//       // const cleared = {
//       //   color: "",
//       //   year: "",
//       //   vin: "",
//       //   model: "",
//       // };
//       // this.setState(cleared);
//     }
//   }

//   async componentDidMount() {
//     const url = "http://localhost:8100/api/models/";
//     const response = await fetch(url);
//     if (response.ok) {
//       const data = await response.json();
//       this.setState({ models: data.models });
//     }
//   }

//   render() {
//     let messageClasses = "alert alert-success d-none mb-0";
//     let formClasses = "";
//     if (this.state.autoCreated) {
//       messageClasses = "alert alert-success mb-0";
//       formClasses = "d-none";
//     }

//     return (
//       <div className="row">
//         <div className="offset-3 col-6">
//           <div className="shadow p-4 mt-4">
//             <h1>Add a new automobile</h1>
//             <form
//               className={formClasses}
//               onSubmit={this.handleSubmit}
//               id="create-automobile-form"
//             >
//               <div className="form-floating mb-3">
//                 <input
//                   onChange={this.handleChange}
//                   value={this.state.color}
//                   placeholder="Color"
//                   required
//                   type="text"
//                   name="color"
//                   id="color"
//                   className="form-control"
//                 />
//                 <label htmlFor="color">Color</label>
//               </div>
//               <div className="form-floating mb-3">
//                 <input
//                   onChange={this.handleChange}
//                   value={this.state.year}
//                   placeholder="Year"
//                   required
//                   type="text"
//                   name="year"
//                   id="year"
//                   className="form-control"
//                 />
//                 <label htmlFor="year">Year</label>
//               </div>
//               <div className="form-floating mb-3">
//                 <input
//                   onChange={this.handleChange}
//                   value={this.state.vin}
//                   placeholder="Vin"
//                   required
//                   type="text"
//                   name="vin"
//                   id="vin"
//                   className="form-control"
//                 />
//                 <label htmlFor="vin">Vin</label>
//               </div>
//               <div className="mb-3">
//                 <select
//                   onChange={this.handleChange}
//                   value={this.state.model}
//                   required
//                   name="model"
//                   id="model"
//                   className="form-select"
//                 >
//                   <option value="">Choose a model</option>
//                   {this.state.models.map((model) => {
//                     return (
//                       <option key={model.id} value={model.id}>
//                         {model.name}
//                       </option>
//                     );
//                   })}
//                 </select>
//               </div>
//               <button className="btn btn-primary">Add automobile</button>
//             </form>
//             <div className={messageClasses} id="success-message">
//               You have added a new automobile to the inventory!
//             </div>
//           </div>
//         </div>
//       </div>
//     );
//   }
// }

// export default AutomobileForm;
