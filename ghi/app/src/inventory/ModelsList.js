import React, { useEffect, useState } from "react";

function ModelsList() {
  const [models, setModels] = useState([]);

  useEffect(() => {
    const getModelData = async () => {
      const modelResponse = await fetch("http://localhost:8100/api/models/");
      const modelData = await modelResponse.json();
      setModels(modelData.models);
    };
    
    getModelData();
  }, []);

  return (
    <>
      <div className="px-4 py-5 my-5 text-center">
        <h1 className="display-5 fw-bold">Models</h1>
          <table className="table table-striped">
            <thead>
              <tr>
                <th scope="col">Name</th>
                <th scope="col">Manufacturer</th>
                <th scope="col">Picture</th>
              </tr>
            </thead>
            <tbody>
              {models.map((model) => {
                return (
                  <tr key={model.id}>
                    <td>{model.name}</td>
                    <td>{model.manufacturer.name}</td>
                    <td>
                      <img src={model.picture_url} alt="car" height="110" />
                    </td>
                  </tr>
                );
              })}
            </tbody>
          </table>
      </div>
    </>
  )
}

export default ModelsList;


// import React from "react";

// function ModelCard(props) {
//   return (
//     <div className="card col-3 my-2 mx-2">
//       {props.model.picture_url && (
//         <img
//           src={props.model.picture_url}
//           className="card-img-top"
//           alt="Model image"
//         />
//       )}
//       <div className="card-body">
//         <h5 className="card-title">{props.model.name}</h5>
//         <h6 className="card-subtitle text-muted">
//           Manufacturer: {props.model.manufacturer.name}
//         </h6>
//       </div>
//       <div className="card-footer">
//         <button
//           onClick={() => {
//             props.onDelete(props.model.id);
//           }}
//           className="btn btn-danger"
//         >
//           Delete
//         </button>
//       </div>
//     </div>
//   );
// }

// class ModelsList extends React.Component {
//   constructor(props) {
//     super(props);

//     this.state = {
//       models: [],
//     };

//     this.deleteModel = this.deleteModel.bind(this);
//   }

//   async deleteModel(id) {
//     const url = `http://localhost:8100/api/models/${id}`;

//     try {
//       const response = await fetch(url, {
//         method: "DELETE",
//       });

//       if (response.ok) {
//         this.setState((prevState) => ({
//           models: prevState.models.filter((model) => model.id !== id),
//         }));
//       }
//     } catch (e) {
//       console.log(e);
//     }
//   }

//   async componentDidMount() {
//     const url = "http://localhost:8100/api/models/";

//     try {
//       const response = await fetch(url);
//       const data = await response.json();

//       if (response.ok) {
//         this.setState({ models: data.models });
//       }
//     } catch (e) {
//       console.log(e);
//     }
//   }

//   render() {
//     return (
//       <div className="px-4 py-5 my-5 text-center">
//         <h1 className="display-5 fw-bold">Models</h1>
//         <div className="col-lg-12 mx-auto">
//           <div className="row justify-content-center">
//             {this.state.models.map((model, index) => {
//               return (
//                 <ModelCard
//                   model={model}
//                   key={model.id}
//                   test={index}
//                   onDelete={this.deleteModel}
//                 />
//               );
//             })}
//           </div>
//         </div>
//       </div>
//     );
//   }
// }

// export default ModelsList;
