import React from "react";

class ManufacturerForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      name: "",
      manufacturerCreated: false,
    };

    this.handleNameChange = this.handleNameChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleNameChange(event) {
    const value = event.target.value;
    this.setState({ name: value });
  }

  async handleSubmit(event) {
    event.preventDefault();
    const data = { ...this.state };
    delete data.manufacturerCreated;

    const manufacturerUrl = "http://localhost:8100/api/manufacturers/";
    const fetchConfig = {
      method: "POST",
      body: JSON.stringify(data),
      headers: {
        "Content-Type": "application/json",
      },
    };

    const response = await fetch(manufacturerUrl, fetchConfig);

    if (response.ok) {
      this.setState({
        name: "",
        manufacturerCreated: true,
      });
    }
  }

  render() {
    let messageClasses = "alert alert-success d-none mb-0";
    let formClasses = "";
    if (this.state.manufacturerCreated) {
      messageClasses = "alert alert-success mb-0";
      formClasses = "d-none";
    }

    return (
      <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Add a new manufacturer</h1>
            <form
              className={formClasses}
              onSubmit={this.handleSubmit}
              id="create-manufacturer-form"
            >
              <div className="form-floating mb-3">
                <input
                  className="form-control"
                  onChange={this.handleNameChange}
                  value={this.state.name}
                  placeholder="Manufacturer name"
                  required
                  type="text"
                  name="name"
                  id="name"
                />
                <label htmlFor="name">Name</label>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
            <div className={messageClasses} id="success-message">
              You have added a new manufacturer!
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default ManufacturerForm;
