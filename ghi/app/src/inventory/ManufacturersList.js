import React from "react";

function ManufacturerTable(props) {
  return (
    <tr key={props.manufacturer.id}>
      <td>{props.manufacturer.name}</td>
      <td>
        <button
          className="btn btn-danger"
          onClick={() => {
            props.onDelete(props.manufacturer.id);
          }}
        >
          Delete
        </button>
      </td>
    </tr>
  );
}

class ManufacturersList extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      manufacturers: [],
    };

    this.deleteManufacturer = this.deleteManufacturer.bind(this);
  }

  async deleteManufacturer(id) {
    const url = `http://localhost:8100/api/manufacturers/${id}`;

    try {
      const response = await fetch(url, {
        method: "DELETE",
      });

      if (response.ok) {
        this.setState((prevState) => ({
          manufacturers: prevState.manufacturers.filter(
            (manufacturer) => manufacturer.id !== id
          ),
        }));
      }
    } catch (e) {
      console.log(e);
    }
  }

  async componentDidMount() {
    const url = "http://localhost:8100/api/manufacturers/";

    try {
      const response = await fetch(url);
      const data = await response.json();

      if (response.ok) {
        this.setState({ manufacturers: data.manufacturers });
      }
    } catch (e) {
      console.log(e);
    }
  }

  render() {
    return (
      <>
        <div className="px-4 py-5 my-5 text-center">
          <h1 className="display-5 fw-bold">Manufacturers</h1>
          <table className="table table-striped">
            <thead>
              <tr>
                <th>Name</th>
                <th>Remove</th>
              </tr>
            </thead>
            <tbody>
              {this.state.manufacturers.map((manufacturer) => {
                return (
                  <ManufacturerTable
                    manufacturer={manufacturer}
                    key={manufacturer.id}
                    onDelete={this.deleteManufacturer}
                  />
                );
              })}
            </tbody>
          </table>
        </div>
      </>
    );
  }
}

export default ManufacturersList;
