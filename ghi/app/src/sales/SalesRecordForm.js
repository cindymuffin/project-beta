import React from "react";

class SalesRecordForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      price: "",
      // this is autos because of the view functions in inventory_rest
      autos: [],
      automobile: "",
      sales_persons: [],
      sales_person: "",
      customers: [],
      customer: "",
      salesRecorded: false,
    };

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChange(event) {
    const newState = {};
    newState[event.target.id] = event.target.value;
    this.setState(newState);
  }

  async componentDidMount() {
    const automobileUrl = "http://localhost:8100/api/automobiles/";
    const salespersonUrl = "http://localhost:8090/api/salespersons/";
    const customerUrl = "http://localhost:8090/api/customers/";
    const soldUrl = "http://localhost:8090/api/salesrecords/";

    const autoResponse = await fetch(automobileUrl);
    const personResponse = await fetch(salespersonUrl);
    const customerResponse = await fetch(customerUrl);
    const soldResponse = await fetch(soldUrl);

    if (
      autoResponse.ok &&
      personResponse.ok &&
      customerResponse.ok &&
      soldResponse.ok
    ) {
      const autoData = await autoResponse.json();
      const salesrepData = await personResponse.json();
      const customerData = await customerResponse.json();
      const soldData = await soldResponse.json();

      // track cars that have been associated with a sales record already
      const forSale = autoData.autos;
      const soldCars = soldData.sales_records;

      // create a list of cars that have not yet been sold
      const soldVins = soldCars.reduce((accum, soldCar) => {
        accum[soldCar.automobile] = true;
        return accum;
      }, {});
      const filteredCars = forSale.filter((unsold) => !soldVins[unsold.vin]);

      this.setState({
        // list only cars that haven't been associated with a sales record
        autos: filteredCars,
        sales_persons: salesrepData.sales_persons,
        customers: customerData.customers,
      });
    }
  }

  async handleSubmit(event) {
    event.preventDefault();
    const data = { ...this.state };
    delete data.autos;
    delete data.sales_persons;
    delete data.customers;
    delete data.salesRecorded;

    const salesrecordUrl = "http://localhost:8090/api/salesrecords/";
    const fetchConfig = {
      method: "POST",
      body: JSON.stringify(data),
      headers: {
        "Content-Type": "application/json",
      },
    };

    const response = await fetch(salesrecordUrl, fetchConfig);
    if (response.ok) {
      this.setState({
        price: "",
        automobile: "",
        autos: [],
        sales_person: "",
        sales_persons: [],
        customer: "",
        customers: [],
        salesRecorded: true,
      });
    }
  }

  render() {
    let messageClasses = "alert alert-success d-none mb-0";
    let formClasses = "";
    if (this.state.salesRecorded) {
      messageClasses = "alert alert-success mb-0";
      formClasses = "d-none";
    }

    return (
      <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Add a new sales record</h1>
            <form
              className={formClasses}
              onSubmit={this.handleSubmit}
              id="create-salesrecord-form"
            >
              <div className="form-floating mb-3">
                <input
                  onChange={this.handleChange}
                  value={this.state.price}
                  placeholder="Price"
                  required
                  type="number"
                  name="price"
                  id="price"
                  className="form-control"
                />
                <label htmlFor="price">Price</label>
              </div>
              <div className="mb-3">
                <select
                  onChange={this.handleChange}
                  value={this.state.automobile}
                  required
                  name="automobile"
                  id="automobile"
                  className="form-select"
                >
                  <option value="">Choose an automobile</option>
                  {/* Again, "autos" instead of "automobile" because of the inventory_rest view function */}
                  {this.state.autos.map((auto) => {
                    return (
                      <option key={auto.href} value={auto.vin}>
                        {auto.year} {auto.color} {auto.model.manufacturer.name}{" "}
                        {auto.model.name} {auto.vin}
                      </option>
                    );
                  })}
                </select>
              </div>
              <div className="mb-3">
                <select
                  onChange={this.handleChange}
                  value={this.state.sales_person}
                  required
                  name="sales_person"
                  id="sales_person"
                  className="form-select"
                >
                  <option value="">Choose a sales person</option>
                  {this.state.sales_persons.map((sales_person) => {
                    return (
                      <option
                        key={sales_person.employee_number}
                        value={sales_person.name}
                      >
                        {sales_person.name} - Employee #
                        {sales_person.employee_number}
                      </option>
                    );
                  })}
                </select>
              </div>
              <div className="mb-3">
                <select
                  onChange={this.handleChange}
                  value={this.state.customer}
                  required
                  name="customer"
                  id="customer"
                  className="form-select"
                >
                  <option value="">Choose a customer</option>
                  {this.state.customers.map((customer) => {
                    return (
                      <option key={customer.phone_number} value={customer.name}>
                        {customer.name} - {customer.phone_number}
                      </option>
                    );
                  })}
                </select>
              </div>
              <button className="btn btn-primary">Add sales record</button>
            </form>
            <div className={messageClasses} id="success-message">
              You have recorded the sale!
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default SalesRecordForm;
