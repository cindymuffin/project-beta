import React from "react";

class SalesHistory extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      sales_persons: [],
      sales_records: [],
      sales_person: "",
    };

    this.handleSalesPersonChange = this.handleSalesPersonChange.bind(this);
  }

  async handleSalesPersonChange(event) {
    const value = event.target.value;
    this.setState({ sales_person: value });

    const salesrecordUrl = "http://localhost:8090/api/salesrecords/";
    const salesrecordResponse = await fetch(salesrecordUrl);

    if (salesrecordResponse.ok) {
      const salesrecordData = await salesrecordResponse.json();

      if (this.state.sales_person === "all") {
        let emptySalesRecordList = [];
        this.setState({ sales_records: emptySalesRecordList });
      } else {
        let filteredSalesRecordList = [];
        for (const salesrecord of salesrecordData.sales_records) {
          if (
            String(salesrecord.sales_person.employee_number) ===
            this.state.sales_person
          ) {
            filteredSalesRecordList.push(salesrecord);
          }
        }

        this.setState({ sales_records: filteredSalesRecordList });
      }
    }
  }

  async componentDidMount() {
    const salesPersonsUrl = "http://localhost:8090/api/salespersons/";
    const salesPersonsResponse = await fetch(salesPersonsUrl);

    if (salesPersonsResponse.ok) {
      const salesPersonsData = await salesPersonsResponse.json();
      this.setState({ sales_persons: salesPersonsData.sales_persons });
    }

    const saleRecordUrl = "http://localhost:8090/api/salesrecords/";
    const saleRecordResponse = await fetch(saleRecordUrl);

    if (saleRecordResponse.ok) {
      let emptySalesRecordList = [];
      this.setState({ sales_records: emptySalesRecordList });
    }
  }

  render() {
    return (
      <div className="px-4 py-5 my-5 text-center">
        <h1 className="display-5 fw-bold">Sales Representative History</h1>
        <div>
          <select
            onChange={this.handleSalesPersonChange}
            value={this.state.sales_person}
            required
            name="sales_person"
            id="sales_person"
            className="form-select"
          >
            <option value="all">Choose a Sales Representative</option>
            {this.state.sales_persons.map((sales_person) => {
              return (
                <option
                  key={sales_person.employee_number}
                  value={sales_person.employee_number}
                >
                  {sales_person.name} - Employee #{sales_person.employee_number}
                </option>
              );
            })}
          </select>
        </div>
        <table className="table table-striped">
          <thead>
            <tr>
              <th>Sales Person</th>
              <th>Customer</th>
              <th>VIN</th>
              <th>Sales Price</th>
            </tr>
          </thead>
          <tbody>
            {this.state.sales_records.map((sales_record) => {
              return (
                <tr key={sales_record.id}>
                  <td>{sales_record.sales_person.name}</td>
                  <td>{sales_record.customer}</td>
                  <td>{sales_record.automobile}</td>
                  <td>${sales_record.price}</td>
                </tr>
              );
            })}
          </tbody>
        </table>
      </div>
    );
  }
}

export default SalesHistory;
