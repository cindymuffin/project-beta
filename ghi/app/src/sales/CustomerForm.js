import React from "react";

class CustomerForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      name: "",
      address: "",
      phone_number: "",
      customerCreated: false,
    };

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChange(event) {
    const newState = {};
    newState[event.target.id] = event.target.value;
    this.setState(newState);
  }

  async handleSubmit(event) {
    event.preventDefault();
    const data = { ...this.state };
    delete data.customerCreated;

    const customerUrl = "http://localhost:8090/api/customers/";
    const fetchConfig = {
      method: "POST",
      body: JSON.stringify(data),
      headers: {
        "Content-Type": "application/json",
      },
    };

    const response = await fetch(customerUrl, fetchConfig);

    if (response.ok) {
      this.setState({
        name: "",
        address: "",
        phone_number: "",
        customerCreated: true,
      });
    }
  }

  render() {
    let messageClasses = "alert alert-success d-none mb-0";
    let formClasses = "";
    if (this.state.customerCreated) {
      messageClasses = "alert alert-success mb-0";
      formClasses = "d-none";
    }

    return (
      <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Add a new customer</h1>
            <form
              className={formClasses}
              onSubmit={this.handleSubmit}
              id="create-customer-form"
            >
              <div className="form-floating mb-3">
                <input
                  className="form-control"
                  onChange={this.handleChange}
                  value={this.state.name}
                  placeholder="Customer name"
                  required
                  type="text"
                  name="name"
                  id="name"
                />
                <label htmlFor="name">Customer Name</label>
              </div>
              <div className="form-floating mb-3">
                <input
                  className="form-control"
                  onChange={this.handleChange}
                  value={this.state.address}
                  placeholder="Customer address"
                  required
                  type="text"
                  name="address"
                  id="address"
                />
                <label htmlFor="picture_url">Customer Address</label>
              </div>
              <div className="form-floating mb-3">
                <input
                  className="form-control"
                  onChange={this.handleChange}
                  value={this.state.phone_number}
                  placeholder="Customer phone number"
                  required
                  type="text"
                  name="phone_number"
                  id="phone_number"
                />
                <label htmlFor="picture_url">Customer Phone Number</label>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
            <div className={messageClasses} id="success-message">
              You have added a new customer!
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default CustomerForm;
