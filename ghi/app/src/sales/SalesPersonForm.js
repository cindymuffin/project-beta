import React from "react";

class SalesPersonForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      name: "",
      employee_number: "",
      employeeCreated: false,
    };

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChange(event) {
    const newState = {};
    newState[event.target.id] = event.target.value;
    this.setState(newState);
  }

  async handleSubmit(event) {
    event.preventDefault();
    const data = { ...this.state };
    delete data.employeeCreated;

    const salespersonUrl = "http://localhost:8090/api/salespersons/";
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        "Content-Type": "application/json",
      },
    };

    const response = await fetch(salespersonUrl, fetchConfig);

    if (response.ok) {
      this.setState({
        name: "",
        employee_number: "",
        employeeCreated: true,
      });
    }
  }

  render() {
    let messageClasses = "alert alert-success d-none mb-0";
    let formClasses = "";
    if (this.state.employeeCreated) {
      messageClasses = "alert alert-success mb-0";
      formClasses = "d-none";
    }

    return (
      <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Add a new sales person</h1>
            <form
              className={formClasses}
              onSubmit={this.handleSubmit}
              id="create-salesperson-form"
            >
              <div className="form-floating mb-3">
                <input
                  className="form-control"
                  onChange={this.handleChange}
                  value={this.state.name}
                  placeholder="Sales person name"
                  required
                  type="text"
                  name="name"
                  id="name"
                />
                <label htmlFor="name">Name</label>
              </div>
              <div className="form-floating mb-3">
                <input
                  className="form-control"
                  onChange={this.handleChange}
                  value={this.state.employee_number}
                  placeholder="Employee number"
                  required
                  type="text"
                  name="employee_number"
                  id="employee_number"
                />
                <label htmlFor="name">Employee Number</label>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
            <br />
            <div className={messageClasses} id="success-message">
              You've added a new sales person!
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default SalesPersonForm;
