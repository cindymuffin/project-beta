import React from "react";

export function SalesTable(props) {
  return (
    <tr>
      <td>{props.sales_record.sales_person.name}</td>
      <td>{props.sales_record.sales_person.employee_number}</td>
      <td>{props.sales_record.customer}</td>
      <td>{props.sales_record.automobile}</td>
      <td>{props.sales_record.price}</td>
    </tr>
  );
}

class SalesList extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      SalesColumn: [],
    };
  }

  async componentDidMount() {
    const url = "http://localhost:8090/api/salesrecords/";

    try {
      const response = await fetch(url);
      if (response.ok) {
        const data = await response.json();
        this.setState({ SalesColumn: data.sales_records });
      }
    } catch (e) {
      console.error(e);
    }
  }

  render() {
    return (
      <div className="px-4 py-5 my-5 text-center">
        <h1 className="display-5 fw-bold">Sales List</h1>
        <table className="table table-striped">
          <thead>
            <tr>
              <th>Sales Representative</th>
              <th>Employee #</th>
              <th>Customer</th>
              <th>VIN</th>
              <th>Sale Price</th>
            </tr>
          </thead>
          <tbody>
            {this.state.SalesColumn.map((salesList, index) => {
              return <SalesTable key={index} sales_record={salesList} />;
            })}
          </tbody>
        </table>
      </div>
    );
  }
}

export default SalesList;
