import React from "react";

export function AppointmentsTable(props) {
  return (
    <table className="table table-striped">
      <thead>
        <tr>
          <th scope="col">VIN</th>
          <th scope="col">Customer Name</th>
          {!props.hideVip && <th scope="col">VIP</th>}
          <th scope="col">Date</th>
          <th scope="col">Time</th>
          <th scope="col">Technician</th>
          <th scope="col">Reason</th>
          {props.showStatus && <th scope="col">Status</th>}
        </tr>
      </thead>
      <tbody>
        {props.appointments.map((appointment) => {
          return (
            <AppointmentsRow
              hideActions={props.hideActions}
              hideVip={props.hideVip}
              showStatus={props.showStatus}
              appointment={appointment}
              key={appointment.id}
              cancel={props.cancelAppointment}
              complete={props.completeAppointment}
            />
          );
        })}
      </tbody>
    </table>
  );
}

export function AppointmentsRow(props) {
  return (
    <tr key={props.appointment.id}>
      <td>{props.appointment.automobile_vin}</td>
      <td>{props.appointment.owner}</td>
      {!props.hideVip && <td>{`${props.appointment.vip}`}</td>}
      <td>{props.appointment.date}</td>
      <td>{props.appointment.time}</td>
      <td>{props.appointment.technician.name}</td>
      <td>{props.appointment.reason}</td>
      {props.showStatus && <td>{props.appointment.status}</td>}
      {!props.hideActions && (
        <>
          <td>
            <button
              className="btn btn-danger"
              onClick={() => {
                props.cancel(props.appointment.id);
              }}
            >
              Cancel
            </button>
          </td>
          <td>
            <button
              className="btn btn-success"
              onClick={() => {
                props.complete(props.appointment.id);
              }}
            >
              Finished
            </button>
          </td>
        </>
      )}
    </tr>
  );
}

class AppointmentList extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      appointments: [],
    };

    this.cancelAppointment = this.cancelAppointment.bind(this);
    this.completeAppointment = this.completeAppointment.bind(this);
  }

  async cancelAppointment(id) {
    const url = `http://localhost:8080/api/appointments/${id}/`;

    const params = {
      status: "Cancelled",
    };

    try {
      const response = await fetch(url, {
        method: "PUT",
        body: JSON.stringify(params),
      });

      if (response.ok) {
        this.setState((prevState) => ({
          appointments: prevState.appointments.filter(
            (appointment) => appointment.id !== id
          ),
        }));
      }
    } catch (e) {
      console.log(e);
    }
  }

  async completeAppointment(id) {
    const url = `http://localhost:8080/api/appointments/${id}/`;

    const params = {
      status: "Complete",
    };

    try {
      const response = await fetch(url, {
        method: "PUT",
        body: JSON.stringify(params),
      });

      if (response.ok) {
        this.setState((prevState) => ({
          appointments: prevState.appointments.filter(
            (appointment) => appointment.id !== id
          ),
        }));
      }
    } catch (e) {
      console.log(e);
    }
  }

  async componentDidMount() {
    const url = "http://localhost:8080/api/appointments/?status=Pending";

    const response = await fetch(url);
    const data = await response.json();

    if (response.ok) {
      this.setState({ appointments: data.appointments });
      console.log(data.appointments);
    }
  }

  render() {
    return (
      <>
        <div className="px-4 py-5 my-5 text-center">
          <h1 className="display-5 fw-bold">Appointments</h1>
          <AppointmentsTable
            appointments={this.state.appointments}
            cancelAppointment={this.cancelAppointment}
            completeAppointment={this.completeAppointment}
          />
        </div>
      </>
    );
  }
}

export default AppointmentList;
