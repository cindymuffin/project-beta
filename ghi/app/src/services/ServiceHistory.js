import React from "react";
import { AppointmentsTable } from "./AppointmentsList";

class ServiceHistory extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      vin: "",
      appointments: [],
    };
    this.handleVinChange = this.handleVinChange.bind(this);
    this.vinSearch = this.vinSearch.bind(this);
  }

  handleVinChange(event) {
    const value = event.target.value;
    this.setState({ vin: value });
  }

  async vinSearch(event) {
    event.preventDefault();
    const searchUrl = `http://localhost:8080/api/appointments/?vin=${this.state.vin}`;
    const fetchConfig = {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
      },
    };

    const response = await fetch(searchUrl, fetchConfig);
    if (response.ok) {
      const data = await response.json();
      this.setState({ appointments: data.appointments });
      console.log(data);
    }
  }

  render() {
    return (
      <div className="row justify-content-center mt-3">
        <form onSubmit={this.vinSearch} className="col-6">
          <div className="input-group mb-3 mt-3">
            <input
              onChange={this.handleVinChange}
              type="text"
              className="form-control"
              placeholder="VIN"
              aria-label="search"
              aria-describedby="basic-addon2"
            />
            <div className="input-group-append">
              <button className="btn btn-outline-secondary" type="submit">
                Search VIN
              </button>
            </div>
          </div>
        </form>
        <AppointmentsTable
          appointments={this.state.appointments}
          hideActions
          hideVip
          showStatus
        />
      </div>
    );
  }
}

export default ServiceHistory;
