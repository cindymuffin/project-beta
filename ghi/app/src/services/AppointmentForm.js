import React from "react";

class AppointmentForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      automobile_vin: "",
      owner: "",
      date: "",
      time: "",
      technician: "",
      technicians: [],
      reason: "",
    };
    this.handleAutomobileVinChange = this.handleAutomobileVinChange.bind(this);
    this.handleOwnerChange = this.handleOwnerChange.bind(this);
    this.handleDateChange = this.handleDateChange.bind(this);
    this.handleTimeChange = this.handleTimeChange.bind(this);
    this.handleTechnicianChange = this.handleTechnicianChange.bind(this);
    this.handleReasonChange = this.handleReasonChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleAutomobileVinChange(event) {
    const value = event.target.value;
    this.setState({ automobile_vin: value });
  }

  handleOwnerChange(event) {
    const value = event.target.value;
    this.setState({ owner: value });
  }

  handleDateChange(event) {
    const value = event.target.value;
    this.setState({ date: value });
  }

  handleTimeChange(event) {
    const value = event.target.value;
    this.setState({ time: value });
  }

  handleTechnicianChange(event) {
    const value = event.target.value;
    this.setState({ technician: value });
  }

  handleReasonChange(event) {
    const value = event.target.value;
    this.setState({ reason: value });
  }

  async handleSubmit(event) {
    event.preventDefault();
    const data = { ...this.state };
    delete data.technicians;

    const appointmentUrl = "http://localhost:8080/api/appointments/";
    const fetchConfig = {
      method: "POST",
      body: JSON.stringify(data),
      headers: {
        "Content-Type": "application/json",
      },
    };

    const response = await fetch(appointmentUrl, fetchConfig);
    if (response.ok) {
      const cleared = {
        automobile_vin: "",
        owner: "",
        date: "",
        time: "",
        technician: "",
        reason: "",
      };
      this.setState(cleared);
    }
  }

  async componentDidMount() {
    const url = "http://localhost:8080/api/technicians/";
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      this.setState({ technicians: data.technicians });
    }
  }

  render() {
    return (
      <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Add appointment</h1>
            <form onSubmit={this.handleSubmit} id="create-appointment-form">
              <div className="form-floating mb-3">
                <input
                  onChange={this.handleAutomobileVinChange}
                  value={this.state.automobile_vin}
                  placeholder="VIN"
                  required
                  type="text"
                  name="automobile_vin"
                  id="automobile_vin"
                  className="form-control"
                />
                <label htmlFor="automobile_vin">VIN</label>
              </div>
              <div className="form-floating mb-3">
                <input
                  onChange={this.handleOwnerChange}
                  value={this.state.owner}
                  placeholder="Owner"
                  required
                  type="text"
                  name="owner"
                  id="owner"
                  className="form-control"
                />
                <label htmlFor="owner">Owner</label>
              </div>
              <div className="form-floating mb-3">
                <input
                  onChange={this.handleDateChange}
                  value={this.state.date}
                  placeholder="Date"
                  required
                  type="date"
                  name="date"
                  id="date"
                  className="form-control"
                />
                <label htmlFor="date">Date</label>
              </div>
              <div className="form-floating mb-3">
                <input
                  onChange={this.handleTimeChange}
                  value={this.state.time}
                  placeholder="Time"
                  required
                  type="time"
                  name="time"
                  id="time"
                  className="form-control"
                />
                <label htmlFor="time">Time</label>
              </div>
              <div className="mb-3">
                <select
                  onChange={this.handleTechnicianChange}
                  value={this.state.technician}
                  required
                  name="technician"
                  id="technician"
                  className="form-select"
                >
                  <option value="">Choose a technician</option>
                  {this.state.technicians.map((technician) => {
                    return (
                      <option key={technician.id} value={technician.id}>
                        {technician.name}
                      </option>
                    );
                  })}
                </select>
              </div>
              <div className="form-floating mb-3">
                <input
                  onChange={this.handleReasonChange}
                  value={this.state.reason}
                  placeholder="Reason"
                  required
                  type="text"
                  name="reason"
                  id="reason"
                  className="form-control"
                />
                <label htmlFor="reason">Reason</label>
              </div>
              <button className="btn btn-primary">Add Appointment</button>
            </form>
          </div>
        </div>
      </div>
    );
  }
}

export default AppointmentForm;
