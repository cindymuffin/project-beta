import { NavLink } from "react-router-dom";

function Nav() {
  return (
    <nav className="navbar navbar-expand-lg navbar-dark bg-success">
      <div className="container-fluid">
        <NavLink className="navbar-brand" to="/">
          CarCar
        </NavLink>
        <button
          className="navbar-toggler"
          type="button"
          data-toggle="collapse"
          data-target="#navbarNavDropdown"
          aria-controls="navbarNavDropdown"
          aria-expanded="false"
          aria-label="Toggle navigation"
        >
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarNavDropdown">
          <ul className="navbar-nav">
            <li className="nav-item dropdown">
              <NavLink
                className="nav-link dropdown-toggle"
                to="#"
                id="navbarDropdownMenuLink"
                data-toggle="dropdown"
                data-bs-toggle="dropdown"
                aria-haspopup="true"
                aria-expanded="false"
              >
                Inventory
              </NavLink>
              <div
                className="dropdown-menu"
                aria-labelledby="navbarDropdownMenuLink"
              >
                <NavLink className="dropdown-item" to="/manufacturers">
                  Manufacturers List
                </NavLink>
                <NavLink className="dropdown-item" to="/manufacturers/new">
                  Add Manufacturer
                </NavLink>
                <NavLink className="dropdown-item" to="/models">
                  Models List
                </NavLink>
                <NavLink className="dropdown-item" to="/models/new">
                  Add Model
                </NavLink>
                <NavLink className="dropdown-item" to="/automobiles">
                  Automobiles List
                </NavLink>
                <NavLink className="dropdown-item" to="/automobiles/new">
                  Add Automobile
                </NavLink>
              </div>
            </li>
            <li className="nav-item dropdown">
              <NavLink
                className="nav-link dropdown-toggle"
                to="#"
                id="navbarDropdownMenuLink"
                data-toggle="dropdown"
                data-bs-toggle="dropdown"
                aria-haspopup="true"
                aria-expanded="false"
              >
                Services
              </NavLink>
              <div
                className="dropdown-menu"
                aria-labelledby="navbarDropdownMenuLink"
              >
                <NavLink className="dropdown-item" to="/appointments">
                  Appointments List
                </NavLink>
                <NavLink className="dropdown-item" to="/appointments/new">
                  Add Appointment
                </NavLink>
                <NavLink className="dropdown-item" to="/servicehistory">
                  Service History
                </NavLink>
                <NavLink className="dropdown-item" to="/technician">
                  Add Technician
                </NavLink>
              </div>
            </li>
            <li className="nav-item dropdown">
              <NavLink
                className="nav-link dropdown-toggle"
                to="#"
                id="navbarDropdownMenuLink"
                data-toggle="dropdown"
                data-bs-toggle="dropdown"
                aria-haspopup="true"
                aria-expanded="false"
              >
                Sales
              </NavLink>
              <div
                className="dropdown-menu"
                aria-labelledby="navbarDropdownMenuLink"
              >
                <NavLink className="dropdown-item" to="/salespersons">
                  Add Sales Person
                </NavLink>
                <NavLink className="dropdown-item" to="/customers">
                  Add Customer
                </NavLink>
                <NavLink className="dropdown-item" to="/sales">
                  Sales List
                </NavLink>
                <NavLink className="dropdown-item" to="/saleshistory">
                  Sales History
                </NavLink>
                <NavLink className="dropdown-item" to="/salesrecords">
                  Add Sales Record
                </NavLink>
              </div>
            </li>
          </ul>
        </div>
      </div>
    </nav>
  );
}

export default Nav;
